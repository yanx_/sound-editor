import audiolib.AudioTrack;
import audiolib.AudioTrackOperation;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.ArrayList;

public class App {
    private static ArrayList<AudioTrack> tracks = new ArrayList<>();
    private ArrayList<AudioTrackOperation> operations = new ArrayList<>();

    public int getCurrentAudio() {
        return currentAudio;
    }

    public static int currentAudio;

    public ArrayList<AudioTrack> getTracks() {
        return tracks;
    }
    public static void setTracks(ArrayList<AudioTrack> list){
        tracks = list;
    }

    public ArrayList<AudioTrackOperation> getOperations() {
        return operations;
    }
    public void addTrack(String filename) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
    tracks.add(new AudioTrack(filename));
        currentAudio = tracks.size() - 1;
    }
    public static void setCurrentAudio(int N){
        if(N >= 0 && N < tracks.size())     currentAudio = N;
        else System.out.println("Invalid number!");
    }

    public static void main(String[] args) throws IOException, LineUnavailableException, UnsupportedAudioFileException {
        App app = new App();
        TUI tui = new TUI(app);
    }
}

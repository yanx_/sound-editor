public enum CommandList {


    //Meta
    HELP("help"), LOAD("load"), OPEN("open"), EXPORT("export"),

    //Nawigacja
    CHECKOUT("checkout"), JUMP("jump"),

    //Odtwarzanie
    PLAY("play"), STOP("stop"),

    //Montaż
    CUT("cut"), COPY("copy"), PASTE("paste"), DELETE("delete"),
    UNDO("undo"), REDO("redo"),

    //Efekty
    FADE_IN("fade-in"), FADE_OUT("fade-out"),
    ;


    private String name;
    private String filename;
    private int id;
    private long timestampIn_ms;
    private int durationInSeconds;
    private int start;
    private int stop;
    private int operation;
    private String destinationToSave;

    public long getTimestampIn_ms() {
        return timestampIn_ms;
    }

    public void setTimestampIn_ms(long timestampIn_ms) {
        this.timestampIn_ms = timestampIn_ms;
    }

    public String getDestinationToSave() {
        return destinationToSave;
    }

    public void setDestinationToSave(String destinationToSave) {
        this.destinationToSave = destinationToSave;
    }

    CommandList(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTimestamp() {
        return timestampIn_ms;
    }

    public void setTimestamp(long timestamp) {
        this.timestampIn_ms = timestamp;
    }

    public int getDurationInSeconds() {
        return durationInSeconds;
    }

    public void setDurationInSeconds(int durationInSeconds) {
        this.durationInSeconds = durationInSeconds;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getStop() {
        return stop;
    }

    public void setStop(int stop) {
        this.stop = stop;
    }

    public int getOperation() {
        return operation;
    }

    public void setOperation(int operation) {
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "CommandList{" +
                "name='" + name + '\'' +
                ", filename='" + filename + '\'' +
                ", id=" + id +
                ", timestampIn_ms=" + timestampIn_ms +
                ", durationInSeconds=" + durationInSeconds +
                ", start=" + start +
                ", stop=" + stop +
                ", operation=" + operation +
                '}';
    }
}

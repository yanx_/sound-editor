import audiolib.*;

import java.io.File;
import java.util.ArrayList;

public class CommandParser {

    AudioTrack audioTrack;
    AudioConcatenator audioConcatenator;
    AudioTrimmer audioTrimmer;
    Converter converter;
    Fader fader;
    ArrayList<AudioTrack> audioList;

    public CommandParser() {
        this.audioConcatenator = new AudioConcatenator();
        this.fader = new Fader();
        this.audioTrimmer = new AudioTrimmer();
        this.converter = new Converter();
        this.audioList = new ArrayList<>();
    }

    public CommandList doCommand(String command) {
        CommandList chosenCommand;
        String[] splitCommand = command.split(" ");
        try {
            chosenCommand = CommandList.valueOf(splitCommand[0].toUpperCase().replace('-', '_'));
        } catch (Exception e) {
            System.out.println("Nieprawidłowa komenda. Spróbuj ponownie.");
            return null;
        }
        int i = 1;
        try {
            switch (chosenCommand) {
                //Meta
                case HELP:
                    System.out.println("[Meta]\n" +
                            "\n" +
                            "- help - wyświetlenie wszystkich dostępnych poleceń\n" +
                            "- exit - wyjście z programu\n" +
                            "\n" +
                            "[Pliki]\n" +
                            "- save - zapis projektu\n" +
                            "- load - wczytanie projektu - operacja niewspierana przez bibliotekę\n" +
                            "- open [filename] - otwarcie pliku w nowej ścieżce audio\n" +
                            "- export [filename] - zapisywanie aktualnie wybranej ścieżki audio do pliku - operacja niewspierana przez bibliotekę\n" +
                            "\n" +
                            "[Nawigacja]\n" +
                            "\n" +
                            "- checkout [id] - zmienia wybraną ścieżkę audio projektu na której pracuje program\n" +
                            "- jump [timestamp] - przesunięcie wskaźnika na podany czas\n" +
                            "\n" +
                            "[Odtwarzanie]\n" +
                            "\n" +
                            "- play (duration) - odtwarza ścieżkę przez dany czas\n" +
                            "- play - kontynuuje odtwarzanie ścieżki przez 3 minuty\n" +
                            "- stop - natychmiast zatrzymuje odtwarzanie ścieżki\n" +
                            "\n" +
                            "[Montaż]\n" +
                            "\n" +
                            "- cut [start, stop] - wycina dany przedział czasowy aktywnej ścieżki\n" +
                            "- copy (start, stop) - kopiuje całą ścieżkę lub dany przedział ścieżki i zapisuje ją w nowej ścieżce audio\n" +
                            "- paste [id] - wklejenie ścieżki o podanym id do aktywnej ścieżki audio w wybranym momencie\n" +
                            "- delete [id] - usuwanie ścieżki o danym id, i wszystkich operacji do niej przypisanych\n" +
                            "- undo (operation) - usuwanie ostatniej lub podanej operacji z kolejki\n" +
                            "- redo - ponowne wykonanie ostatnio cofniętej operacji\n" +
                            "\n" +
                            "[Efekty]\n" +
                            "\n" +
                            "- fade-in (start, stop) - zastosowanie efektu fade in\n" +
                            "- fade-out (start, stop) - zastosowanie efektu fade out\n" +
                            "\n" +
                            "[Opis parametrów]\n" +
                            "\n" +
                            "- filename - nazwa pliku, zawiera rozszerzenie\n" +
                            "- id - id ścieżki audio, wyrażone liczbą\n" +
                            "- timestamp - format hh:mm:ss.xxx - określa konkretny punkt ścieżki audio\n" +
                            "- duration - format liczba+suffiks, gdzie suffiksem może być \"s\" oznaczające sekundy lub \"m\" oznaczające minuty\n" +
                            "- start, stop - format taki sam jak timestamp\n" +
                            "- operation - numer operacji, liczba wybrana spośród wyświetlonych na liście operacji");
                    break;
                case LOAD:  //TODO: brak metodu open
                    chosenCommand.setFilename(splitCommand[i]);
                    i++;
                    audioTrack = new AudioTrack(chosenCommand.getFilename());
                    audioConcatenator.setParameters(audioTrack);
                    audioList.add(audioTrack);
                    App.setTracks(audioList);

                    break;
                case OPEN:
                    chosenCommand.setFilename(splitCommand[i].trim());
                    i++;
                    audioTrack.play();
                    break;
                case EXPORT:
                    chosenCommand.setDestinationToSave(splitCommand[i]);
                    i++;
                    if(chosenCommand.getDestinationToSave().substring(
                            chosenCommand.getDestinationToSave().length()-4,chosenCommand.getDestinationToSave().length()).equals(".mp3")){
                        converter.wavToMp3(new File(chosenCommand.getFilename()),new File(chosenCommand.getDestinationToSave()));

                        }else if(chosenCommand.getDestinationToSave().substring(
                            chosenCommand.getDestinationToSave().length()-4,chosenCommand.getDestinationToSave().length()).equals(".wav")){
                        converter.mp3ToWav(new File(chosenCommand.getFilename()),new File(chosenCommand.getDestinationToSave()));
                    }
                    audioTrack.save(chosenCommand.getDestinationToSave());
                    break;


                //Nawigacja
                case CHECKOUT:
                    chosenCommand.setId(Integer.parseInt(splitCommand[i].trim()));
                    i++;
                    audioTrack = audioList.get(chosenCommand.getId());
                    App.setCurrentAudio(chosenCommand.getId());
                    App.setTracks(audioList);
                    break;
                case JUMP:
                    chosenCommand.setTimestamp(parseJump(splitCommand[i]));
                    i++;
                    audioTrack.jump(chosenCommand.getTimestampIn_ms());
                    App.setTracks(audioList);
                    break;

                //Odtwarzenie
                case PLAY: // dwa przypadki
                    audioTrack.play();
                    break;
                case STOP:
                    audioTrack.stop();
                    break;

                //Montaż
                case CUT:
                    chosenCommand.setStart((int) parseJump(splitCommand[i]));
                    i++;
                    chosenCommand.setStop((int) parseJump(splitCommand[i]));
                    i++;
                    chosenCommand.setDestinationToSave(splitCommand[i]);
                    i++;
                    audioTrimmer.setAudioTrack(audioTrack);
                    audioTrimmer.setParameters(chosenCommand.getStart(),chosenCommand.getStop());
                    audioTrack = audioTrimmer.apply();
                    audioTrack.save(chosenCommand.getDestinationToSave());
                    break;
                case COPY:
                    if(splitCommand.length == 4){
                        chosenCommand.setStart((int) parseJump(splitCommand[i]));
                        i++;
                        chosenCommand.setStop((int) parseJump(splitCommand[i]));
                        i++;
                    }else
                    chosenCommand.setDestinationToSave(splitCommand[i]);
                    i++;
                    audioTrack = new AudioTrack(chosenCommand.getFilename());
                    audioTrack.save(chosenCommand.getDestinationToSave());
                    audioList.add(audioTrack);
                    App.setTracks(audioList);
                    break;
                case PASTE://TODO: brak w bibliotece
                    chosenCommand.setId(Integer.parseInt(splitCommand[i]));
                    i++;
                    chosenCommand.setDestinationToSave(splitCommand[i]);
                    i++;
                    break;
                case DELETE:
                    chosenCommand.setId(Integer.parseInt(splitCommand[i]));
                    i++;
                   audioList.remove(chosenCommand.getId());
                    break;
                case UNDO:
                    chosenCommand.setFilename(splitCommand[i]);
                    i++;
                    chosenCommand.setDestinationToSave(splitCommand[i]);
                    i++;
                    break;
                case REDO: //TODO: dokończyć
                    chosenCommand.setFilename(splitCommand[i]);
                    i++;
                    chosenCommand.setDestinationToSave(splitCommand[i]);
                    i++;
                    break;

                //Efekty
                case FADE_IN:
                    chosenCommand.setStart((int) parseJump(splitCommand[i]));
                    i++;
                    chosenCommand.setStop((int) parseJump(splitCommand[i]));
                    i++;
                    chosenCommand.setDestinationToSave(splitCommand[i]);
                    i++;
                    fader.fadeAudio(audioTrack.getFilename(),chosenCommand.getDestinationToSave(),
                            0,chosenCommand.getStart()-chosenCommand.getStop());
                    break;
                case FADE_OUT:
                    chosenCommand.setStart((int) parseJump(splitCommand[i]));
                    i++;
                    chosenCommand.setStop((int) parseJump(splitCommand[i]));
                    i++;

                    chosenCommand.setDestinationToSave(splitCommand[i]);
                    i++;
                    fader.fadeAudio(audioTrack.getFilename(),chosenCommand.getDestinationToSave(),
                            (chosenCommand.getStart()-chosenCommand.getStop()),0);
                    break;

                //Default
                default:
                    System.out.println("Nieprawidłowa komenda. Spróbuj ponownie.");
            }
            if (i == splitCommand.length) {
                return chosenCommand;
            } else return null;

        } catch (Exception e) { //TODO usunąć printstactrace
            System.out.println("Parsowanie nie powiodło się");
        }
        return null;
    }

    private static long parseJump(String string) {
        string.trim();
        String[] tab = string.split(":");
        String[] tab1 = tab[2].split("\\.");
        tab[2] = tab1[0];
        long timeInMili = 0;
        long converter = 360000;
        for (int i = 0; i < tab.length; i++) {
            timeInMili = timeInMili + ((Long.parseLong(tab[i])) * converter);
            converter = converter / 60;
        }
        try {
            timeInMili = timeInMili + Long.parseLong(tab1[1]);
        }catch (Exception e ){
        }
        return timeInMili;
    }

    private int parseDuration(String string) {
        string.trim();
        String type = string.substring(string.length() - 1);
        String vale = string.substring(0, string.length() - 1);
        if (type.equals("s")) {
            return Integer.parseInt(vale);
        } else if (type.equals("m")) {
            return Integer.parseInt(vale) * 60;
        }
        return 0;
    }
}

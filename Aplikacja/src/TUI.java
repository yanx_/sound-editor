import audiolib.AudioTrack;
import audiolib.AudioTrackOperation;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.graphics.Scrollable;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class TUI {
    private Scrollable scrollable;
    private String command;
    private Terminal terminal = null;
    private final TextGraphics textGraphics;
    private App app;
    public static String message;
    CommandParser commandParser;
    public TUI(App app) throws IOException, LineUnavailableException, UnsupportedAudioFileException {
        this.commandParser = new CommandParser();
        DefaultTerminalFactory defaultTerminalFactory = new DefaultTerminalFactory();
        this.app = app;
        terminal = new DefaultTerminalFactory().createTerminal();
        terminal.enterPrivateMode();
        Screen screen = new TerminalScreen(terminal);
        screen.startScreen();
        textGraphics = terminal.newTextGraphics();
        textGraphics.setForegroundColor(TextColor.ANSI.WHITE);
        textGraphics.setBackgroundColor(TextColor.ANSI.BLACK);
        editor();
    }
    public void editor() throws IOException, LineUnavailableException, UnsupportedAudioFileException {
        try {
            ArrayList<AudioTrack> tracks = app.getTracks();
            if (!app.getTracks().isEmpty()) {
                textGraphics.putString(0, 0, "Tracks:");
                terminal.putCharacter('\n');
                for (int i = 0; i < app.getTracks().size(); i++) {
                    AudioTrack track = tracks.get(i);
                    String[] h = track.getFilename().split("/");
                    String filename = h[h.length - 1];
                    if (i == app.getCurrentAudio()) terminal.putCharacter('*');
                    terminal.putString("[id: " + i + " filename: " + filename + "]");
                    terminal.putCharacter('\n');
                    terminal.putString("[" + timeFormat(track.getCurrentTime()) + "/" + timeFormat(track.getDuration()) + "]");
                    terminal.putCharacter('\n');
                    terminal.putCharacter('[');
                    int progressBar = (int) (78 * track.getCurrentTime() / track.getDuration());
                    for (int j = 0; j < progressBar; j++) {
                        terminal.putCharacter('=');
                    }
                    terminal.putCharacter('>');
                    for (int j = progressBar + 1; j < 78; j++) {
                        terminal.putCharacter('-');
                    }
                    terminal.putCharacter(']');
                    terminal.putCharacter('\n');
                }
                terminal.putCharacter('\n');
                terminal.putCharacter('\n');
            }
            ArrayList<AudioTrackOperation> operations = app.getOperations();
            if (!operations.isEmpty()) {
                terminal.putString("Operations:");
                terminal.putCharacter('\n');
                for (int i = 0; i < operations.size(); i++) {
                    terminal.putString(operations.toString());
                    terminal.putCharacter('\n');
                }
                terminal.putCharacter('\n');
                terminal.putCharacter('\n');
            }
            terminal.putString("Command-line:");
            terminal.putCharacter('\n');
            terminal.putString("> ");

            terminal.flush();
            StringBuilder input = new StringBuilder();
            int col = 2;
            while(true) {
                 KeyStroke keyStroke = terminal.pollInput();
                if (keyStroke == null) {
                    terminal.flush();
                    keyStroke = terminal.readInput();
                } if (keyStroke.getKeyType() == KeyType.Escape || keyStroke.getKeyType() == KeyType.EOF) {
                    break;
                } else if(keyStroke.getKeyType() == KeyType.Enter){
                    command = input.toString();
                    input = new StringBuilder();
                    col = 2;
                    commandParser.doCommand(command);
                    //textGraphics.drawLine(terminal.getCursorPosition(), new TerminalPosition(1, terminal.getCursorPosition().getRow()), ' ');
                    terminal.clearScreen();
                    editor();
                } else if(keyStroke.getKeyType() == KeyType.Backspace){
                    if(col > 2) {
                        textGraphics.setCharacter(terminal.getCursorPosition().getColumn() - 1, terminal.getCursorPosition().getRow(), ' ');
                        terminal.setCursorPosition(--col, terminal.getCursorPosition().getRow());
                        terminal.flush();
                        input.deleteCharAt(input.length() - 1);
                    }
                }
                else if(keyStroke.getKeyType() == KeyType.Character){
                    textGraphics.setCharacter(terminal.getCursorPosition().getColumn(), terminal.getCursorPosition().getRow(), keyStroke.getCharacter());
                    terminal.setCursorPosition(++col, terminal.getCursorPosition().getRow());
                    input.append(keyStroke.getCharacter());
                    terminal.flush();
                }
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        finally {
            if(terminal != null) {
                try {
                    terminal.close();
                }
                catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String timeFormat(long millis) {
        String val = String.format("%02d:%02d:%02d.%03d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)),
                millis % 1000);
        return val;
    }
}

